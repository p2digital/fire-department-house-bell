exports.index = function(req, res){

	var config = require("../config");
	var inbox = require('inbox');
	var control = require("./control");

	var MailParser = require("mailparser").MailParser,
    	mailparser = new MailParser(),
    	fs = require("fs");

	var client = inbox.createConnection(false, "mail.armonkfd.com", {
		secureConnection: true,
		auth: {
			user: config.email_account,
			pass: config.email_password
		}
	});


	client.connect();

	client.on("connect", function(){
		console.log("connect success");
		client.listMailboxes(function(err, mboxs){
			client.openMailbox("INBOX", function(err, info){
				client.listMessages(-1, 0, function(err, messages){
					if(messages[0] == undefined){
						console.log('no messages');
						client.close();
					}else{
						var uid = messages[0].UID;
						var messageStream = client.createMessageStream(uid).pipe(mailparser);
						mailparser.on("end", function(mail_object){
						    var call_type = process_call_type(mail_object.text);
						    client.deleteMessage(uid, function(){
						    	client.close();
						    });
						});
					}
					
				});
			})
		})
	})

	client.on("close", function(){
		console.log('disconnected');
	})



	var process_call_type = function(message){
		var type_string = message.match("Type:(.*), Time out:")
		control.process_call(type_string[1]);
	}




}