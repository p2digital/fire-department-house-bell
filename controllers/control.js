

//Display all the lights that you have with their name and ID
exports.view_all = function(req, res){
	var request = require('request');

	request('http://10.0.1.14/api/nodetestuser/lights', function(error, response, body){
		res.render('index', {body: body})
	});
}


//Display a specific light status by ID number
// - /control/status/:num/
exports.view_light = function(req, res){
	var request = require('request');

	var light_number = req.params.num;
	request('http://10.0.1.14/api/nodetestuser/lights/'+light_number, function(error, response, body){
		res.render('index', {body: body})
	});
}

//Manually control a light based on settings you pass in below 
// - /control/status/:num/manual/
exports.manual_control = function(req, res){
	var request = require('request');
	
	var light_number = req.params.num;
	var status = req.params.stat;

	request({
		method: "PUT",
		uri:'http://10.0.1.14/api/nodetestuser/lights/'+light_number+'/state',
		json: true,
		body:{"on":true, "sat":255, "bri":255,"hue":10000},
	}, function(error, response, body){
		res.render('index', {body: JSON.stringify(body)})
	});

}


//Process the call and gest the call type. Turns on specified light number to set options below. After 10 seconds the light changes back to the way it was. 
exports.process_call = function(type){

	console.log("Type: ", type)
	var light_number = 2;


	var request = require('request');

	request('http://10.0.1.14/api/nodetestuser/lights/'+light_number, function(error, response, body){
		
		var light_data = JSON.parse(body);
			light_data = light_data.state;


		if(type == "ALS, " || type == "BLS, "){
			var light_info = {
				"on" : true,
				"sat" : 253,
				"bri" : 254,
				"hue" :47124
			}
		}else if(type == "ALARM, COMM" || type == "ALARM, CO" || type == "INVEST, ELECT" || type == "ALARM, RESD" || type == "MVA, INJURIES" || type == "MVA, EXT"){
			var light_info = {
				"on" : true,
				"sat" : 253,
				"bri" : 254,
				"hue" :65527
			}
		}else if(type == "SERVICE, WATER" || type == "SERVICE, APPLIANC"){
			console.log("TYPE2")
			var light_info = {
				"on" : true,
				"sat" : 251,
				"bri" : 221,
				"hue" :18473
			}
		}else if(type == "STRU, "){
			var light_info = {
				"on" : true,
				"sat" : 253,
				"bri" : 254,
				"hue" :65527,
				"alert" : "lselect",

			}
		}


		//Set the state of the light
		request({
			method: "PUT",
			uri:'http://10.0.1.14/api/nodetestuser/lights/'+light_number+'/state',
			json: true,
			body:light_info,
		}, function(error, response, body){
			//PUt the light back to how it was after 1 min. 
			setTimeout(function(){
				request({
					method: "PUT",
					uri:'http://10.0.1.14/api/nodetestuser/lights/'+light_number+'/state',
					json: true,
					body:{"on":light_data.on, "sat":light_data.sat, "bri":light_data.bri,"hue":light_data.hue},
				}, function(error, response, body){
					//PUt the light back to how it was after 1 min. 
					console.log('light put back the way it was')
				});

			}, 10000)
			
		});

	});



}