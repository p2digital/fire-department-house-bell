module.exports = function(app){


	var connect = require("./controllers/connect");
	app.get("/connect", connect.index);


	var control = require("./controllers/control");
	app.get("/control/view_all/", control.view_all);
	app.get("/control/status/:num/", control.view_light);
	app.get("/control/status/:num/manual/", control.manual_control);


	var email_check = require("./controllers/emails");
	app.get("/emails/check", email_check.index);
	
	setTimeout(function(){
		console.log('init')
		email_check.index();
	}, 1000)
	
	setInterval(function(){
		console.log('running interval')
		email_check.index();
	}, 45 * 1000)

}